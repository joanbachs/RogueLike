﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    private static GameManager _instance = null;
    private int _score;
    private int _bestScore;
    private int _coins;
    private int _enemiesOnScene;

    public int Score { get => _score; set => _score = value; }
    public int BestScore { get => _bestScore; set => _bestScore = value; }
    public int Coins { get => _coins; set => _coins = value; }
    public int EnemiesOnScene { get => _enemiesOnScene; set => _enemiesOnScene = value; }


    public GameObject Player;
    public GameObject UI;
    private RoomManager RM;

    public static GameManager Instance
    {
        get { return _instance; }
    }


    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;

        Score = 0;
        BestScore = 0;
        EnemiesOnScene = 0;
        Coins = PlayerPrefs.GetInt("Coins");
    }

    private void Update()
    {
        //if (SpawnController.Instance.AllEnemiesSpawned)
        //{
        //    if (EnemiesOnScene <= 0)
        //    {
        //        //if (nextRoomCollider)
        //        //{
        //        //    RM.nextRoom();
        //        //    nextRoomCollider = false;
        //        //}

        //        //loadResultScene();
        //    }
        //}

        if (Score > BestScore)
        {
            BestScore = Score;
            PlayerPrefs.SetInt("HighestScore", BestScore);
        }
        PlayerPrefs.SetInt("Coins", Coins);
    }

    public void IncreaseScore(int score)
    {
        Score += score;
    }

    public void increaseEnemiesNumber()
    {
        EnemiesOnScene++;
    }

    public void reduceEnemiesNumber()
    {
        EnemiesOnScene--;
    }

    public void loadResultScene()
    {
        SceneManager.LoadScene(1);
    }

}
