﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public DataPlayer dp;
    private SpriteRenderer spriteRenderer;
    private Rigidbody2D rb;
    private float speed;

    private float horizontal;
    private float vertical;

    public Transform playerTransform;

    private Vector2 direction = Vector2.right;
    public float DashForce;
    public float StartDashTimer;
    public float DashCooldown;
    private bool DashReady;
    private bool isDashing;
    float CurrentDashTimer;

    private float timerDashCooldown = 5f;
    public bool canBeDamaged;

    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        rb = GetComponent<Rigidbody2D>();
        speed = 7f;
        DashReady = true;
        DashCooldown = timerDashCooldown;
        canBeDamaged = true;
    }


    // Update is called once per frame
    void Update()
    {
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");

        //moveHorizontal();
        //moveVertical();
        Move();
        if (Input.GetKeyDown(KeyCode.LeftShift) && DashReady)
        {
            isDashing = true;
            CurrentDashTimer = StartDashTimer;
            rb.velocity = Vector2.zero;
            DashReady = false;
        }

        if (isDashing)
        {
            canBeDamaged = false;
            CurrentDashTimer -= Time.deltaTime;
            rb.AddForce(direction.normalized * DashForce, ForceMode2D.Impulse);
            if (CurrentDashTimer <= 0)
            {
                isDashing = false;
                rb.velocity = Vector2.zero;
            }
            DashCooldown = timerDashCooldown;
        }
        else
        {
            canBeDamaged = true;
        }

        if (DashCooldown <= 0)
        {
            DashReady = true;
        }
        else
        {
            DashCooldown -= Time.deltaTime;
        }      
    }

    void Move()
    {
        if (horizontal != 0 || vertical != 0)
        {
            direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            transform.Translate(direction*Time.deltaTime*speed);
            //rb.AddForce(direction * speed, ForceMode2D.Force);
        }
        if (horizontal == 0 && vertical == 0)
        {
            rb.velocity = Vector2.zero;
        }
    }
    /*
    private void FixedUpdate()
    {
        if (horizontal != 0 || vertical != 0)
        {
            direction = new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            rb.AddForce(direction * speed, ForceMode2D.Force);
        }
    }
    */
    
    
        //private void moveHorizontal()
        //{
        //    if (Input.GetKeyDown(KeyCode.A))
        //    {
        //        if (!isFlippedX)
        //        {
        //            FlipX();
        //        }
        //    }
        //    if (Input.GetKeyDown(KeyCode.D))
        //    {
        //        if (isFlippedX)
        //        {
        //            FlipX();
        //        }
        //    }

        //    if (isFlippedX)
        //    {
        //        //rb.MovePosition(forceX);
        //        transform.Translate(-horizontal * Time.deltaTime * speed, 0f, 0f);
        //    }
        //    else if (!isFlippedX)
        //    {
        //        //rb.MovePosition(new Vector2(horizontal * Time.deltaTime * speed, 0f));
        //        transform.Translate(horizontal * Time.deltaTime * speed, 0f, 0f);
        //    }

        //}
        //private void moveVertical()
        //{
        //    if (Input.GetKeyDown(KeyCode.W))
        //    {
        //        if (!isFlippedY)
        //        {
        //            FlipY();
        //        }
        //    }
        //    if (Input.GetKeyDown(KeyCode.S))
        //    {
        //        if (isFlippedY)
        //        {
        //            FlipY();
        //        }
        //    }

        //    if (isFlippedY)
        //    {
        //        //rb.MovePosition(forceX);
        //        transform.Translate(0f, -vertical * Time.deltaTime * speed, 0f);
        //    }
        //    else if (!isFlippedY)
        //    {
        //        //rb.MovePosition(new Vector2(horizontal * Time.deltaTime * speed, 0f));
        //        transform.Translate(0f, vertical * Time.deltaTime * speed, 0f);
        //    }
        //}
    
/*
    void FlipX()
    {
        transform.Rotate(0f, 180f, 0f);
        isFlippedX = !isFlippedX;
    }
    void FlipY()
    {
        transform.Rotate(180f, 0f, 0f);
        isFlippedY = !isFlippedY;
    }
*/
}
