﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wepon : MonoBehaviour
{
    private Transform firePoint;
    public Rigidbody2D bulletPrefab;
    [SerializeField] private float bulletSpeed;
    private Vector2 dir;

    void Start()
    {
        firePoint = gameObject.GetComponent<Transform>();
    }

    private void Update()
    {
        dir = Camera.main.ScreenToWorldPoint(Input.mousePosition) - transform.position;
        float angle = Mathf.Atan2(dir.y, dir.x) * Mathf.Rad2Deg;
        Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = rotation;

        if (Input.GetKeyDown(KeyCode.Mouse0))
        {
            Shoot();
        }
    }

    private void Shoot()
    {
        //var bullet = Instantiate(bulletPrefab, firePoint.position, Quaternion.identity);
        //bullet.GetComponent<Bullet>().FirePoint = firePoint;

        Rigidbody2D instance = Instantiate(bulletPrefab, transform.position, Quaternion.identity);
        dir.Normalize();
        instance.velocity = dir * bulletSpeed;
    }
}
