﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultUI : MonoBehaviour
{
    [SerializeField] private Text resultText;
    [SerializeField] private Text bestScoreText;

    // Start is called before the first frame update
    void Start()
    {
        resultText.text ="Game score: "+ GameManager.Instance.Score.ToString() + " pts";
        bestScoreText.text ="Best score: " + GameManager.Instance.BestScore.ToString() + " pts";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
