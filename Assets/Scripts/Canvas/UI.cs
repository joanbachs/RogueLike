﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class UI : MonoBehaviour
{
    public Image healthBar;
    private DataPlayer dp;
    public static float health;
    void Start()
    {
        dp = GameManager.Instance.Player.GetComponent<DataPlayer>();
        health = dp.CurrentHp; 
    }

    void Update()
    {
        health = dp.CurrentHp;
        float hp = health / dp.MaxHealth;
        healthBar.fillAmount = hp;
    }

}
