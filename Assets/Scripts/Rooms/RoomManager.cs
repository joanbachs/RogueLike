﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomManager : MonoBehaviour
{
    [SerializeField] private GameObject[] rooms;
    private int actualRoom = 0;

    public delegate void ChangeRoom();
    public static event ChangeRoom OnRoomChange;

    public delegate void EndRoom();
    public static event EndRoom OnRoomEnd;

    private int enemiesOnScene;

    public BoxCollider2D exitCollider;
    public GameObject environmentSpawner;

    // Start is called before the first frame update
    void Start()
    {
        OnRoomChange += countEnemies;
        OnRoomChange += nextRoom;
        exitCollider = GetComponent<BoxCollider2D>();
        exitCollider.enabled = false;
    }

    void FixedUpdate()
    {
        countEnemies();
        if (SpawnController.Instance.AllEnemiesSpawned)
        {
            if (enemiesOnScene <= 0)
            {
                exitCollider.enabled = true;
                roomFinished();
            }
        }
    }

    public void nextRoom()
    {
        GameObject lastRoom = GameObject.FindGameObjectWithTag("Room");

        if (lastRoom != null)
        {
            Destroy(lastRoom);
        }
        actualRoom = Random.Range(0, rooms.Length);
        clean();
        instantiateRoom();
        exitCollider.enabled = false;
    }

    public void roomFinished()
    {
        if (OnRoomEnd != null)
        {
            OnRoomEnd();
        }
    }

    private void countEnemies()
    {
        enemiesOnScene = GameManager.Instance.EnemiesOnScene;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.tag == "Player")
        {
            Debug.Log("player touching door");
            if (OnRoomChange != null)
            {
            OnRoomChange();
            }
        }
    }

    void instantiateRoom()
    {
        Instantiate(rooms[actualRoom], new Vector3(0,0,0), Quaternion.identity);
        Instantiate(environmentSpawner, new Vector3(0,0,0), Quaternion.identity);
        Debug.Log(actualRoom);
    }

    void clean()
    {
        GameObject[] kits = GameObject.FindGameObjectsWithTag("MaxHealth");
        foreach (GameObject kit in kits)
            GameObject.Destroy(kit);

        GameObject[] medikits = GameObject.FindGameObjectsWithTag("ExtraHealth");
        foreach (GameObject kit in medikits)
            GameObject.Destroy(kit);
    }
}
