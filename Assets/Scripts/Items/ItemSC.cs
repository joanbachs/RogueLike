﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ItemSC : ScriptableObject
{
    public string name;
    public int price;
    public type itemType;
    public Sprite icon;

    public enum type
    {
        Weapon,
        PowerUp
    }
}
