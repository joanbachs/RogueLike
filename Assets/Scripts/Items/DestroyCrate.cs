﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestroyCrate : MonoBehaviour
{
    public GameObject[] dropItemArray = new GameObject[2];

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Bullet"))
        {
            int drop = Random.Range(0,2);
            if (drop == 1)
            {
                dropItem();
            }
            Destroy(gameObject);
        }
    }

    private void dropItem()
    {
        int item = Random.Range(0, 2);
        Instantiate(dropItemArray[item], transform.position, Quaternion.identity);
    }

}
