﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnController : MonoBehaviour
{
    [SerializeField]
    private GameObject[] enemy = new GameObject[2];

    float spawnRate = 3f;
    float nextSpawn = 0.0f;
    float randY;
    float randX;
    int randomEnemy;
    Vector2 whereToSpawn;

    public bool AllEnemiesSpawned = false;
    [SerializeField] private int totalEnemiesNum;
    private int enemiesSpawned;

    private static SpawnController _instance = null;

    public static SpawnController Instance
    {
        get { return _instance; }
    }
    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        }
        _instance = this;

    }
    // Start is called before the first frame update
    void Start()
    {
        enemiesSpawned = 0;
        AllEnemiesSpawned = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (!AllEnemiesSpawned)
        {
            if (Time.time > nextSpawn)
            {
                nextSpawn = Time.time + spawnRate;
                randY = Random.Range(-8f, 4.2f);
                randX = Random.Range(-15f, 14f);
                randomEnemy = (int)Random.Range(0, 2);
                whereToSpawn = new Vector2(randX, randY);
                Instantiate(enemy[randomEnemy], whereToSpawn, Quaternion.identity);
                GameManager.Instance.increaseEnemiesNumber();
                enemiesSpawned++;
            }
        }

        if (enemiesSpawned >= totalEnemiesNum)
        {
            AllEnemiesSpawned = true;
        }
    }
}
